<?
/**/
/** ВНИМАНИЕ!
* Этот файл сгенерирован автоматически и не подлежит редактированию.
* Исходники лежат в папке templates/src/jade
*/
/**/
?><?
define('PAGE_TITLE','Мишки из роз оптом');
define('PAGE_SECTION_ID','0');
define('PAGE_JS_VIEW','wholesale');
?>
<div data-view="<?=PAGE_JS_VIEW?>" class="page page--wholesale">
  <div id="section-1" class="wholesale"><img src="/templates/build/images/wholesale/price--arrow.png" alt="" data-aos="fade" data-aos-delay="500" data-aos-duration="1500" class="wholesale--arrow"/><img src="/templates/build/images/wholesale/price.png" alt="" data-aos="fade" data-aos-delay="1000" data-aos-duration="1500" class="wholesale--price-list"/><img src="/templates/build/images/wholesale/logo.png" alt="" data-aos="fade-left" data-aos-delay="0" data-aos-duration="1000" class="wholesale--logo"/>
    <div class="container wholesale--container">
      <h1 class="wholesale--heading">ОРИГИНАЛЬНЫЕ МИШКИ ИЗ РОЗ<span class="wholesale--heading__small">от&nbsp;<b>The Rose Bear</b></span></h1>
      <h2 class="wholesale--description"><span class="brown">ОПТОМ </span>в наличиии со склада в Москве <br/>и под заказ&nbsp;<b>от&nbsp;</b><span class="wholesale--price">590 ₽</span></h2>
      <h2 class="form__wholesale__thanks">Мы отправили Вам прайс лист</h2>
      <form data-aos="fade-left" data-aos-delay="500" data-aos-duration="1500" class="form__wholesale">
        <div class="form__wholesale--heading">Оставьте заявку и получите самый актуальный прайс <br/>на сегодня
          <input type="text" name="type" value=" - " class="input__hidden input__type"/>
        </div>
        <label class="input--label">
          <p class="input--heading">Введите Ваше имя</p>
          <input type="text" name="name" placeholder="Ваше имя" class="input--item input__name"/>
        </label>
        <label class="input--label">
          <p class="input--heading">Введите Ваш номер телефона&nbsp;<span class="red">*</span></p>
          <input type="text" name="phone" placeholder="+7 (___) ___-__-__" required="required" class="input--item input__phone"/>
        </label>
        <label class="input--label">
          <p class="error">Введите email в формате "mail@mailbox.ru"</p>
          <p class="input--heading">Введите Ваш E-mail&nbsp;<span class="red">*</span></p>
          <input type="text" name="email" placeholder="Ваш E-mail" required="required" class="input--item input__email"/>
        </label>
        <input type="submit" value="Заказать прайс сейчас!" class="btn btn__yellow input--send"/>
      </form>
    </div>
  </div>
  <div id="section-2" class="wholesale--about"><img src="/templates/build/images/wholesale/s-2_left.png" alt="" data-aos="fade-up" data-aos-delay="500" data-aos-duration="2500" class="wholesale--about--img__left"/><img src="/templates/build/images/wholesale/s-2_bg.png" alt="" class="wholesale--about--img__bg"/>
    <div class="container wholesale--about--container">
      <h2 data-aos="fade-right" data-aos-delay="100" data-aos-duration="1000" class="wholesale--about--heading">5 ПРИЧИН<br/>почему именно мы Ваши надежные поставщики</h2>
      <ul class="wholesale--about--content">
        <li data-aos="flip-left" data-aos-delay="300" data-aos-duration="700" class="wholesale--about--item">
          <div class="wholesale--about--item--img"><img src="/templates/build/images/wholesale/about-icon-1.png" alt=""/></div>
          <p class="wholesale--about--item--description"><b>Минимальная партия</b>всего от 16 шт. (1&nbsp;коробка)</p>
        </li>
        <li data-aos="flip-left" data-aos-delay="500" data-aos-duration="700" class="wholesale--about--item">
          <div class="wholesale--about--item--img"><img src="/templates/build/images/wholesale/about-icon-2.png" alt=""/></div>
          <p class="wholesale--about--item--description"><b>Выполнены исключительно</b>из высококачественных материалов</p>
        </li>
        <li data-aos="flip-left" data-aos-delay="700" data-aos-duration="700" class="wholesale--about--item">
          <div class="wholesale--about--item--img"><img src="/templates/build/images/wholesale/about-icon-3.png" alt=""/></div>
          <p class="wholesale--about--item--description"><b>Прямые поставки </b>с завода-изготовителя в Гонконге</p>
        </li>
        <li data-aos="flip-left" data-aos-delay="900" data-aos-duration="700" class="wholesale--about--item">
          <div class="wholesale--about--item--img"><img src="/templates/build/images/wholesale/about-icon-4.png" alt=""/></div>
          <p class="wholesale--about--item--description"><b>Каждый мишка бережно упакован</b>в оригинальный подарочный пакет с изображением сотен алых сердец</p>
        </li>
        <li data-aos="flip-left" data-aos-delay="1100" data-aos-duration="700" class="wholesale--about--item">
          <div class="wholesale--about--item--img"><img src="/templates/build/images/wholesale/about-icon-5.png" alt=""/></div>
          <p class="wholesale--about--item--description"><b>Любые дополнительные аксессуары</b>в наличии и под заказ - подарочные боксы, ленты и др.</p>
        </li>
      </ul>
    </div>
  </div>
  <div id="section-3" class="wholesale--contacts"><img src="/templates/build/images/wholesale/s-2_bg_2.png" alt="" class="wholesale--contacts--img__bg"/>
    <div id="map" data-aos="fade-right" data-aos-delay="500" data-aos-duration="2000" class="map">
      <h2 class="map--heading container map--container">Наши контакты</h2>
      <p class="map--description container map--container">СКЛАД в Москве : 5 мин. от метро Шоссе Энтузиастов</p>
      <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A1a322f14098d49baa9fcf11bd16f3a2db4790953fe5f495748662328f75fa559&amp;source=constructor" width="100%" height="500" frameborder="0" class="map--frame"></iframe>
    </div>
  </div>
</div>