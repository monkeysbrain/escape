<?
/**/
/** ВНИМАНИЕ!
* Этот файл сгенерирован автоматически и не подлежит редактированию.
* Исходники лежат в папке templates/src/jade
*/
/**/
?><?
define('PAGE_TITLE','Escape. We can do that');
define('PAGE_SECTION_ID','0');
define('PAGE_JS_VIEW','Index');
?>
<div data-view="<?=PAGE_JS_VIEW?>" class="page page--index">
  <section id="section-1" class="intro">
    <h1 data-aos="zoom-in" data-aos-delay="0" data-aos-duration="1000" class="intro--heading"><span class="intro--heading__small">Can you build the website of my dreams?</span><span class="intro--heading__big">Yup, we can do that.</span></h1><a href="#form" data-aos="zoom-in" data-aos-delay="0" data-aos-duration="1000" class="btn intro--btn">Learn More</a>
  </section>
  <section id="section-2" data-aos="fade-right" data-aos-delay="10" data-aos-duration="1000" class="webdev">
    <h2 class="webdev--heading">Web <span class="bold">Development</span></h2>
    <div class="webdev--description">
      <p>Proin iaculis purus consequat sem cure digni ssim. Donec porttitora entum suscipit aenean rhoncus posuere odio in tincidunt. Proin iaculis purus consequat sem cure digni ssim. Donec porttitora entum suscipit aenean rhoncus posuere odio in tincidunt.</p>
      <p>Iaculis purus consequat sem cure digni ssim. Donec porttitora entum suscipit aenean rhoncus posuere odio in tincidunt. Proin iaculis purus consequat sem cure digni ssim.</p>
    </div>
  </section>
  <section id="section-3" data-aos="fade-left" data-aos-delay="10" data-aos-duration="1000" class="branding--wrapper">
    <div class="branding">
      <h2 class="branding--heading">Identity <span class="bold">Branding</span></h2>
      <div class="branding--description">
        <p>Proin iaculis purus consequat sem cure digni ssim. Donec porttitora entum suscipit aenean rhoncus posuere odio in tincidunt. Proin iaculis purus consequat sem cure digni ssim. Donec porttitora entum suscipit.</p>
        <p>Iaculis purus consequat sem cure digni ssim. Donec porttitora entum suscipit aenean rhoncus posuere odio in tincidunt. Proin iaculis purus consequat.</p>
      </div>
    </div>
  </section>
  <section id="section-4" class="benefits">
    <article data-aos="zoom-in" data-aos-delay="100" data-aos-duration="800" class="benefits--item">
      <div class="benefits--img--wrapper"><img src="/templates/build/images/content/s3-a1.png" alt="" class="benefits--img"/></div>
      <p class="benefits--description"><span class="benefits--heading">Branding & Identity</span>Proin iaculis purus consequat sem cure  digni ssim. Donec porttitora entum suscipit  aenean rhoncus posuere odio in tincidunt. </p>
    </article>
    <article data-aos="zoom-in" data-aos-delay="400" data-aos-duration="800" class="benefits--item">
      <div class="benefits--img--wrapper"><img src="/templates/build/images/content/s3-a2.png" alt="" class="benefits--img"/></div>
      <p class="benefits--description"><span class="benefits--heading">Web & Graphic Design</span>Proin iaculis purus consequat sem cure digni ssim. Donec porttitora entum suscipit aenean rhoncus posuere odio in tincidunt. </p>
    </article>
    <article data-aos="zoom-in" data-aos-delay="700" data-aos-duration="800" class="benefits--item">
      <div class="benefits--img--wrapper"><img src="/templates/build/images/content/s3-a3.png" alt="" class="benefits--img"/></div>
      <p class="benefits--description"><span class="benefits--heading">Mobile App Development</span>Proin iaculis purus consequat sem cure digni ssim. Donec porttitora entum suscipit aenean rhoncus posuere odio in tincidunt. </p>
    </article>
    <article data-aos="zoom-in" data-aos-delay="1000" data-aos-duration="800" class="benefits--item">
      <div class="benefits--img--wrapper"><img src="/templates/build/images/content/s3-a4.png" alt="" class="benefits--img"/></div>
      <p class="benefits--description"><span class="benefits--heading">Animations</span>Proin iaculis purus consequat sem cure digni ssim. Donec porttitora entum suscipit aenean rhoncus posuere odio in tincidunt. </p>
    </article>
    <article data-aos="zoom-in" data-aos-delay="1300" data-aos-duration="800" class="benefits--item">
      <div class="benefits--img--wrapper"><img src="/templates/build/images/content/s3-a5.png" alt="" class="benefits--img"/></div>
      <p class="benefits--description"><span class="benefits--heading uppercase">ui/ux</span>Proin iaculis purus consequat sem cure digni ssim. Donec porttitora entum suscipit aenean rhoncus posuere odio in tincidunt. </p>
    </article>
    <article data-aos="zoom-in" data-aos-delay="1600" data-aos-duration="800" class="benefits--item">
      <div class="benefits--img--wrapper"><img src="/templates/build/images/content/s3-a6.png" alt="" class="benefits--img"/></div>
      <p class="benefits--description"><span class="benefits--heading">Photography</span>Proin iaculis purus consequat sem cure digni ssim. Donec porttitora entum suscipit aenean rhoncus posuere odio in tincidunt. </p>
    </article>
  </section>
  <section id="form" class="feedback--wrapper">
    <div class="feedback">
      <div data-aos="flip-right" data-aos-delay="0" data-aos-duration="1500" class="feedback--info">
        <h2 class="feedback--heading">Say <span class="bold">Hello</span></h2>
        <div class="feedback--description">
           
          Don’t be shy, drop us an email and say hello! We are a really nice bunch of people :)
        </div>
        <section class="contacts"><a href="tel:84165550000" class="contacts--item"><i class="fas fa-mobile-alt"></i>
            <p class="contacts--info">(416) 555-0000</p></a><a href="mailto:hello@escape.com" class="contacts--item"> <i class="fas fa-envelope"></i>
            <p class="contacts--info">hello@escape.com</p></a><a href="https://twitter.com/escape" _blank="_blank" class="contacts--item"><i class="fab fa-twitter"></i>
            <p class="contacts--info">@escape</p></a><a href="https://facebook.com/escape" _blank="_blank" class="contacts--item"><i class="fab fa-facebook-f"></i>
            <p class="contacts--info">facebook.com/escape</p></a><a href="https://plus.com/escape" _blank="_blank" class="contacts--item"><i class="fab fa-google-plus-g"></i>
            <p class="contacts--info">plus.com/escape</p></a><a href="https://pinterest.com/escape" _blank="_blank" class="contacts--item"><i class="fab fa-pinterest-p"></i>
            <p class="contacts--info">pinterest.com/escape</p></a></section>
      </div>
      <form data-aos="flip-left" data-aos-delay="0" data-aos-duration="1500" class="feedback--form">
        <input type="text" name="name" placeholder="Your Name *" required="required" class="input ajaxName"/>
        <input type="email" name="email" placeholder="Your E-mail*" required="required" class="input ajaxEmail"/>
        <textarea name="message" placeholder="Your Message *" class="ajaxMessage"></textarea>
        <input type="submit" value="Send form" class="btn input--btn"/>
      </form>
    </div>
  </section>
</div>