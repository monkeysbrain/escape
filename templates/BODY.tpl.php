<?
/**/
/** ВНИМАНИЕ!
* Этот файл сгенерирован автоматически и не подлежит редактированию.
* Исходники лежат в папке templates/src/jade
*/
/**/
?><!DOCTYPE html>
<html>
  <head>
        <meta charset="utf-8">
        <base href="<?=SITE_URL?>">
        <title><?=PAGE_TITLE?></title>
        <meta content="Интернет м-н, оригинальных мишек из роз, ручная работа" name="description">
        <meta content="Интернет м-н, оригинальных мишек из роз, ручная работа" name="keywords">
        <link rel="image_src">
        <meta name="author">
        <meta content="telephone=no" name="format-detection">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="HandheldFriendly" content="true">
        <meta name="MobileOptimized" content="320">
        <meta content="IE=edge" http-equiv="X-UA-Compatible">
        <meta property="og:title" content="Интернет м-н, оригинальных мишек из роз, ручная работа">
        <meta property="og:image" content="">
        <meta property="og:site_name" content="bears">
        <meta property="og:description" content="Интернет м-н, оригинальных мишек из роз, ручная работа">
        <meta name="twitter:title" content="Интернет м-н, оригинальных мишек из роз, ручная работа">
        <meta name="twitter:image:src" content="">
        <meta name="twitter:description" content="Интернет м-н, оригинальных мишек из роз, ручная работа">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon.ico">
        <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css">
        <link href="<?=urlV('templates/build/css/styles'.$sourceSuffix.'.css')?>" rel="stylesheet" type="text/css">
  </head>
  <body>
    <script src="<?=urlV("templates/build/js/init".$sourceSuffix.".js")?>"></script>
    <header class="header">
      <nav data-aos="fade-down" data-aos-delay="0" data-aos-duration="1500" class="header--nav"><a href="#section-1" class="header--logo"><img src="/templates/build/images/content/logo.png" alt="logo" class="header--logo--img"></a>
        <div class="header--nav--list"><a href="#section-1" class="header--nav--item">Home</a><a href="#section-2" class="header--nav--item">Services</a><a href="#section-3" class="header--nav--item">Clients</a><a href="#section-4" class="header--nav--item">Team</a><a href="#form" class="header--nav--item">Contact</a></div>
      </nav>
    </header>
    <main class="grid"><?=$CONTENT?></main>
    <div class="loader js-loader">
      <div class="loader_box"></div>
    </div>
    <footer data-aos="fade-up" data-aos-delay="0" data-aos-duration="1500" class="footer">
      <div class="copy">© 2014 Escape by FreebiesXpress • Made In Toronto</div>
    </footer>
    <script src="<?=urlV("templates/build/js/libs".$sourceSuffix.".js")?>"></script>
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script src="<?=urlV("templates/build/js/scripts".$sourceSuffix.".js")?>"></script>
    <script src="/templates/build/vendors/jquery.maskedinput.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
  </body>
</html>