app.ns('app.views').Index = app.core.Page.extend({

    events: {},


    initialize: function() {
        app.views.Index.__super__.initialize.apply(this, arguments)
    },

    remove: function() {
        app.views.Index.__super__.remove.apply(this, arguments)
    },

    render: function() {
        app.views.Index.__super__.render.apply(this, arguments)

        app.views.Index.__super__.afterRender.apply(this, arguments)

    }
})