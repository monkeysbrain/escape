'use strict'

app.ns('app.views').Header = app.core.View.extend({

    el: '.header',
    subView: {},

    initialize: function() {

    },

    render: function() {
        app.views.Header.__super__.render.apply(this, arguments);

        function _stickyHeader() {
            var $header = $('.header');
            var $headerHeight = $header.height();
            var $introHeight = $('.intro').height() + $headerHeight;
            $(window).scroll(function() {
                var $scroll = $(window).scrollTop();
                $scroll > $introHeight ? $header.addClass('header__sticky') : $header.removeClass('header__sticky');
            })
        }

        function _validateForm() {
            var $send = $('.input--btn');
            var $wrapper = $('.feedback--wrapper');
            var $input = $('.ajaxName, .ajaxEmail, .ajaxMessage');
            var $name = $('.ajaxName');
            var $mail = $('.ajaxEmail');
            let $test = /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/;
            var $message = $('.ajaxMessage');
            $name.on('keyup', function() {
                ($name.val() !== '') ? $name.addClass('isValid').removeClass('notValid'): $name.removeClass('isValid').addClass('notValid');
            });

            $message.on('keyup', function() {
                ($message.val() !== '') ? $message.addClass('isValid').removeClass('notValid'): $message.removeClass('isValid').addClass('notValid');
            });

            $mail.on('keyup', function() {
                $test.test($mail.val()) ? $mail.addClass('isValid').removeClass('notValid') : $mail.removeClass('isValid').addClass('notValid');
            });

            $mail.on('blur', function() {
                $(this).hasClass('notValid') ? $(this).addClass('wrongEmail').attr('placeholder', 'Enter email in right format (name@domain.com)').val('') : $(this).removeClass('wrongEmail').attr('placeholder', 'Your E-mail*')
            });

            $input.on('keyup', function() {
                if ($('.isValid').length === 3) {
                    $wrapper.addClass('feedback--wrapper__send')
                    setTimeout(function() {
                        $send.fadeIn()
                    }, 500)
                } else {
                    $wrapper.removeClass('feedback--wrapper__send')
                    setTimeout(function() {
                        $send.fadeOut()
                    }, 500)
                }
            })

            AOS.init();
        }

        _stickyHeader()
        _validateForm()

    }
})